﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_damage_controll : MonoBehaviour {

    public int dmg = 0;
    public float life_time = 2.0f;

    // Use this for initialization
    void Start () {
            
        //少ししたら消える
        Destroy(this.GetComponentInParent<Canvas>().gameObject, life_time);
        
    }

    // Update is called once per frame
    void Update () {

        //呼び出し元で格納されたダメージを表示
        this.GetComponent<Text>().text = dmg.ToString();

    }
    
}
