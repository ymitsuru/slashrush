﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_controll : MonoBehaviour,Damageable {
    public Rigidbody2D rb;
    public float speed_rate = 1.0f;
    public float brake_rate = 1.0f;
    public float add_speed_rate = 0.1f;
    public float now_speed_rate = 0.0f;
    public float now_speed_brake_rate = 0.3f;
    public float flick_range_time = 0.8f;
    public int flick_range_distance = 30;
    public float blow_distance_rate = 1.0f;
    public float blow_distance_base = 10.0f;
    public float player_back_distance = 12000;
    public float damage_ui_offset = 1.5f;
    public GameObject effect_hit_prefab;        //Inspectorで設定
    public GameObject gm;        //Inspectorで設定
    private Vector2 touchStartPos;
    private Vector2 touchEndPos;
    private float touchTime;
    private string direction_result = "init";
    private Animator child_animator;
    private Transform player_transform;
    private Vector3 tempPos;
    private float temp_mag;
    private Enum_MoveDir player_dir;
    private bool touch_on = false;  //そのフレームでタッチをしているかどうか判定用

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody2D>();
        child_animator = GetComponentInChildren<Animator>();
        player_transform = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update () {

        //プレイヤーの移動方向を取得
        if (rb.velocity.x > 0)
        {
            //右向きに移動中
            player_dir = Enum_MoveDir.RIGHT;
        }
        else if (rb.velocity.x < 0)
        {
            //左向きに移動中
            player_dir = Enum_MoveDir.LEFT;
        }
        else
        {
            //停止中
            player_dir = Enum_MoveDir.STAY;
        }

        //プレイヤーがある程度右に行ったら戻す（デバッグ用）
        if (player_transform.position.x > player_back_distance)
        {
            tempPos = player_transform.position;
            tempPos.x = 0;
            player_transform.position = tempPos;
        }

        //プレイヤーの速度ベクトルをアニメーション用変数に格納
        child_animator.SetFloat("Speed", rb.velocity.magnitude);

        
        // タッチ継続中
        if (touch_on)
        {
            //フリック判定用タイマーカウント
            touchTime += Time.deltaTime;

            //ブレーキ中は減速
            if (child_animator.GetCurrentAnimatorStateInfo(0).IsName("Player_brake_loop"))
            {
                //右方向に進んでいたら
                if (player_dir == Enum_MoveDir.RIGHT)
                {
                    //逆方向に加速（ブレーキ）
                    rb.AddForce(Vector2.left * brake_rate * Time.deltaTime, ForceMode2D.Impulse);

                }

                //加速度減少
                now_speed_rate -= Time.deltaTime * now_speed_rate;

            }

        }

        //何も押されていないとき
        if (!touch_on)
        {
            //ダメージ中でなければ
            if(!child_animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
            {
                //加速値を加算
                now_speed_rate += Time.deltaTime * add_speed_rate;

                //徐々に加速
                rb.AddForce(Vector2.right * Time.deltaTime * (speed_rate + now_speed_rate), ForceMode2D.Impulse);
            }

            //ブレーキモーション中だったら
            if (child_animator.GetCurrentAnimatorStateInfo(0).IsName("Player_brake_loop"))
            {
                child_animator.SetTrigger("To_Dash");                
            }
            
        }

              
        /*

        // タッチした瞬間
        if (Input.GetMouseButtonDown(0) ||   //PC用判定行：左クリック押下時
             (Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Began))    // スマホ用判定行：タッチ
        {
            
        }
        // タッチ継続中
        else if (Input.GetMouseButton(0)  //PC用判定行：左クリック押し続け中
                 || (Input.touchCount > 0) && ((Input.GetTouch(0).phase == TouchPhase.Moved) || (Input.GetTouch(0).phase == TouchPhase.Stationary)))    // スマホ用判定行：ドラッグ中
        {
        
        }
        // タッチリリース時
        else if (Input.GetMouseButtonUp(0)   //PC用判定行：左クリックリリース時
                 || (Input.touchCount > 0) && (Input.GetTouch(0).phase == TouchPhase.Ended))     // スマホ用判定行：タッチリリース時
        {
         
        }
        //何も押されていないとき
        else
        {
         
        }
        
    */
          

    }


    // タッチした瞬間
    public void ScreenTouch()
    {

        //タッチしているか判定用のフラグ
        touch_on = true;

        touchStartPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        //フリック判定用タイマーリセット
        touchTime = 0.0f;

        //ダメージ中でなければ
        if (!child_animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
        {
            //突きブレーキ開始
            child_animator.SetTrigger("Charge_Start");

        }

    }

    // タッチリリース時
    public void ScreenRelease()
    {

        //タッチしているか判定用のフラグ
        touch_on = false;

        touchEndPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

        //どの操作か判定・処理
        InputCheck();

        //フリック判定用タイマーリセット
        touchTime = 0.0f;

    }
    


    //フリック方向判定用
    void InputCheck(){
        float directionX = touchEndPos.x - touchStartPos.x;
        float directionY = touchEndPos.y - touchStartPos.y;

        if (Mathf.Abs(directionY) < Mathf.Abs(directionX))
        {
            if (flick_range_distance < directionX)
            {
                //右向きにフリック
                direction_result = "right";
            }
            else if ( -flick_range_distance > directionX)
            {
                //左向きにフリック
                direction_result = "left";
            }
        }
        else if (Mathf.Abs(directionX) < Mathf.Abs(directionY))
        {
            if (flick_range_distance < directionY)
            {
                //上向きにフリック
                direction_result = "up";
            }
            else if (-flick_range_distance > directionY){
                //下向きのフリック
                direction_result = "down";
            }
        }
        else if(touchTime > flick_range_time)
        {
            //ロングタッチを検出
            direction_result = "long_touch";

        }
        else
        {
            //タッチを検出
            direction_result = "touch";

        }


        switch (direction_result)
        {
            case "up":
                //上フリックされた時の処理
                Debug.Log(direction_result);
                break;

            case "down":
                //下フリックされた時の処理
                Debug.Log(direction_result);
                break;

            case "right":
                //右フリックされた時の処理
                Debug.Log(direction_result);


                break;

            case "left":
                //左フリックされた時の処理
                Debug.Log(direction_result);

                break;

            case "touch":
                //タッチされた時の処理
                Debug.Log(direction_result);

                if (!child_animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
                {
                    //斬りモーション呼び出し
                    child_animator.SetTrigger("Attack");

                }

                break;

            case "long_touch":
                //ロングタッチされた時の処理
                Debug.Log(direction_result);

                //ダメージ中でなければ
                if (!child_animator.GetCurrentAnimatorStateInfo(0).IsName("damage"))
                {
                    //斬りモーション呼び出し
                    child_animator.SetTrigger("Attack");

                }

                break;

        }
    }

    //攻撃した側のオブジェクトから呼び出される
    public void Damage()
    {

    }

    public void Damage(int type, int atk, float speed)
    {
        //ダメージリアクション呼び出し
        child_animator.SetTrigger("Damage");

        //直前のスピードを保存（吹き飛び距離に反映）
        temp_mag = rb.velocity.magnitude;

        //速度リセット
        rb.velocity = new Vector2(0, 0);

        //加速値をリセット
        now_speed_rate = 0.0f;

        //吹き飛び
        rb.AddForce(Vector2.left * ((blow_distance_rate * temp_mag) + blow_distance_base), ForceMode2D.Impulse);

        Instantiate(effect_hit_prefab, this.gameObject.transform);

        //ダメージUIを呼び出し
        gm.GetComponent<Game_Manager>().Damage_UI_ON((int)(atk * (speed + temp_mag)) + atk, this.transform.position, damage_ui_offset, type);

    }



}
